﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Attachments;
using OH.DAL.Repositories.Attachments;
using OH.Net;

namespace MyOH.Net.Tests.Attachments
{
    /// <summary>
    /// Summary description for AttachmentRepositoryTests
    /// </summary>
    [TestClass]
    public class AttachmentRepositoryTests
    {
        private IAttachmentRepository _attachmentRepository;
        private Attachment _attachment;
        private static string _base64String;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext AttachmentRepositoryTestsContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
         [ClassInitialize]
        public static void AttachmentRepositoryTests_Initialize(TestContext testContext)
        {
            var inFile = new StreamReader(@"G:\OHDevelopment\Websites\MyOH.Net\MyOH.Net.Tests\Attachments\test.txt",
                 System.Text.Encoding.ASCII);

            var base64CharArray = new char[inFile.BaseStream.Length];

            inFile.Read(base64CharArray, 0, (int)inFile.BaseStream.Length);

            _base64String = new string(base64CharArray);                    
            
         }
        

        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        // public static void AttachmentRepositoryTests_Cleanup() 
        //{ 
        //    //To avoid creating very large data sets
        //}
        
        // Use TestInitialize to run code before running each test 
         [TestInitialize]
        public void AttachmentRepositoryTests_Initialize() 
         {
             _attachmentRepository = UnityConfig.GetConfiguredContainer().Resolve<IAttachmentRepository>();
             _attachment = new Attachment { FileName = "test.pdf", FileExtension = ".pdf", FileStream = Convert.FromBase64String(_base64String) };
             
         }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
         //[TestMethod]
         //public void GetAllEntitiesByGuid()
         //{
         //    var id = _attachmentRepository.GetAllEntitiesByGuid(Attachment.ClassGuid);             

         //    Assert.IsFalse(false);
         //}

        [TestMethod]
         public void AttachmentRepositoryTests_Save()
        {
            _attachmentRepository.AddOrUpdate(_attachment);
            _attachmentRepository.Save();

            var entity = _attachmentRepository.GetById(_attachment.Id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void AttachmentRepositoryTests_Update()
        {
            _attachmentRepository.AddOrUpdate(_attachment);
            _attachmentRepository.Save();

            var originalEntityValue = _attachment.FileName;
            _attachment.FileName = "updatedTest.txt";

            _attachmentRepository.AddOrUpdate(_attachment);
            var entity = _attachmentRepository.GetById(_attachment.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.FileName));
        }

        [TestMethod]
        public void AttachmentRepositoryTests_GetById()
        {
            var firstOrDefault = _attachmentRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _attachmentRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void AttachmentRepositoryTests_GetAll()
        {
            var entityList = _attachmentRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }      
    }
}
