﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Attachments;
using OH.Net;

namespace MyOH.Net.Tests.Attachments
{
    /// <summary>
    ///     Summary description for FileTypeRepositoryTests
    /// </summary>
    [TestClass]
    public class FileTypeRepositoryTests
    {
        private FileType _fileType;
        private IFileTypeRepository _fileTypeRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext FileTypeRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize]
        public static void FileTypeRepositoryTests_Initialize(TestContext testContext)
        {
        }


        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void FileTypeRepositoryTests_Cleanup()
        {
            //To avoid creating very large data sets
        }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void FileTypeRepositoryTests_Initialize()
        {
            _fileTypeRepository = UnityConfig.GetConfiguredContainer().Resolve<IFileTypeRepository>();

            _fileType = new FileType { Description = "Dummy File", Extension = "txt", CssClass = "text" };

            var fileTypeList = new List<FileType>
            {   new FileType {Description = "PDF File", Extension = "pdf", CssClass = "pdf"},
                new FileType {Description = "Windows folder", CssClass = "rootfolder"},
                new FileType {Description = "Windows folder", CssClass = "folder"},
                new FileType {Description = "HTML Document", Extension = "html", CssClass = "html"},
                new FileType {Description = "JPG Image", Extension = "jpg", CssClass = "image"},
                new FileType {Description = "JPEG Image", Extension = "jpeg", CssClass = "image"},
                new FileType {Description = "PNG Image", Extension = "png", CssClass = "image"}
            };

            foreach (var fileType in fileTypeList)
            {
                _fileTypeRepository.AddOrUpdate(fileType);
                _fileTypeRepository.Save();
            }



        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        //[TestMethod]
        //public void GetAllEntitiesByGuid()
        //{

        //}

        [TestMethod]
        public void FileTypeRepositoryTests_Save()
        {
            _fileTypeRepository.AddOrUpdate(_fileType);
            _fileTypeRepository.Save();

            var entity = _fileTypeRepository.GetById(_fileType.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void FileTypeRepositoryTests_Update()
        {
            _fileTypeRepository.AddOrUpdate(_fileType);
            _fileTypeRepository.Save();

            _fileType.Description = "Simple Text File";

            _fileTypeRepository.AddOrUpdate(_fileType);

            var entity = _fileTypeRepository.GetById(_fileType.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void FileTypeRepositoryTests_GetById()
        {
            var firstOrDefault = _fileTypeRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _fileTypeRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void FileTypeRepositoryTests_GetAll()
        {
            var entityList = _fileTypeRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}