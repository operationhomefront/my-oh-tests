﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Persons;
using OH.DAL.Domain.Phones;
using OH.DAL.Repositories.Persons;
using OH.Net;

namespace MyOH.Net.Tests.Persons
{
    /// <summary>
    ///     Summary description for PersonPhoneRepositoryTests
    /// </summary>
    [TestClass]
    public class PersonPhoneRepositoryTests
    {
        private PersonPhone _personPhone;
        private IPersonPhoneRepository _personPhoneRepository;        
        private Person _person;
        private Phone _phone;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext PersonPhoneRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void PersonPhoneRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void PersonPhoneRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void PersonPhoneRepositoryTests_Initialize()
        {
            _personPhoneRepository = UnityConfig.GetConfiguredContainer().Resolve<IPersonPhoneRepository>();
            

            _personPhone = new PersonPhone();
            _person = new Person { FirstName = "Hilario" };
            _phone = new Phone { PhoneNumber = "210-123-4564" };




            _personPhone = new PersonPhone
            {
                Person = _person,
                Phone = _phone
            };

        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void PersonPhoneRepositoryTests_Save()
        {
            _personPhoneRepository.AddOrUpdate(_personPhone);
            _personPhoneRepository.Save();

            var entity = _personPhoneRepository.GetById(_personPhone.Id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void PersonPhoneRepositoryTests_Update()
        {
            _personPhoneRepository.AddOrUpdate(_personPhone);
            _personPhoneRepository.Save();


            //Set this to nested entity, and update the name or another property
            var originalEntityValue = _personPhone.Phone.PhoneNumber;

            var phone = new Phone { PhoneNumber = "210-999-1234" };


            //Set a new nested entity id
            _personPhone.Phone = phone;


            _personPhoneRepository.AddOrUpdate(_personPhone);

            var entity = _personPhoneRepository.GetByIdInclude(_personPhone.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Phone.PhoneNumber));
        }

        [TestMethod]
        public void PersonPhoneRepositoryTests_GetById()
        {
            var firstOrDefault = _personPhoneRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _personPhoneRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void PersonPhoneRepositoryTests_GetAll()
        {
            var entityList = _personPhoneRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

