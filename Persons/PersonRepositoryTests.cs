﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories.Persons;
using OH.Net;

namespace MyOH.Net.Tests.Persons
{
    /// <summary>
    ///     Summary description for PersonRepositoryTests
    /// </summary>
    [TestClass]
    public class PersonRepositoryTests
    {
        private Person _person;
        private IPersonRepository _personRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext PersonRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void PersonRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void PersonRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void PersonRepositoryTests_Initialize()
        {
            _personRepository = UnityConfig.GetConfiguredContainer().Resolve<IPersonRepository>();

            _person = new Person {FirstName = "Hilario", Lastname = "Urquieta"};

            //var personList = new List<Person>
            //{   new Person {Description = "PDF File", Extension = "pdf", CssClass = "pdf"},
            //    new Person {Description = "Windows folder", CssClass = "rootfolder"},
            //    new Person {Description = "Windows folder", CssClass = "folder"},
            //    new Person {Description = "HTML Document", Extension = "html", CssClass = "html"},
            //    new Person {Description = "JPG Image", Extension = "jpg", CssClass = "image"},
            //    new Person {Description = "JPEG Image", Extension = "jpeg", CssClass = "image"},
            //    new Person {Description = "PNG Image", Extension = "png", CssClass = "image"}
            //};

            //foreach (var person in personList)
            //{
            //    _personRepository.AddOrUpdate(person);
            //    _personRepository.Save();
            //}



        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        //[TestMethod]
        //public void GetAllEntitiesByGuid()
        //{

        //}

        [TestMethod]
        public void PersonRepositoryTests_Save()
        {
             _personRepository.AddOrUpdate(_person);
            _personRepository.Save();

            var entity = _personRepository.GetById(_person.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void PersonRepositoryTests_Update()
        {
            _personRepository.AddOrUpdate(_person);
            _personRepository.Save();
            
            var originalFirstName = _person.FirstName;
            _person.FirstName = "Alberto";            

            _personRepository.AddOrUpdate(_person);

            var entity = _personRepository.GetById(_person.Id);

            Assert.IsFalse(originalFirstName.Equals(entity.FirstName));
        }

        [TestMethod]
        public void PersonRepositoryTests_GetById()
        {
            var firstOrDefault = _personRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _personRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void PersonRepositoryTests_GetAll()
        {
            var entityList = _personRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

