﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Addresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories.Persons;
using OH.Net;

namespace MyOH.Net.Tests.Persons
{
    /// <summary>
    ///     Summary description for PersonAddressRepositoryTests
    /// </summary>
    [TestClass]
    public class PersonAddressRepositoryTests
    {
        private PersonAddress _personAddress;
        private IPersonAddressRepository _personAddressRepository;
        private Person _person;
        private Address _address;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext PersonAddressRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void PersonAddressRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void PersonAddressRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void PersonAddressRepositoryTests_Initialize()
        {
            _personAddressRepository = UnityConfig.GetConfiguredContainer().Resolve<IPersonAddressRepository>();

            _personAddress = new PersonAddress();
            _person = new Person{FirstName = "Hilario"};
            _address = new Address{Address1 = "1232 Nowhere Ln"};

            _personAddress = new PersonAddress
               {
                   Person = _person,
                   Address = _address
               };

        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void PersonAddressRepositoryTests_Save()
        {
            var id = _personAddressRepository.AddOrUpdate(_personAddress);
            _personAddressRepository.Save();

            var entity = _personAddressRepository.GetById(id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void PersonAddressRepositoryTests_Update()
        {
            _personAddressRepository.AddOrUpdate(_personAddress);
            _personAddressRepository.Save();


            //Set this to nested entity, and update the name or another property
            var originalEntityValue = _personAddress.Address.Address1;

            var address = new Address { Address1 = "1232 Highway 48" };

            //Set a new nested entity id
            _personAddress.Address = address;


            _personAddressRepository.AddOrUpdate(_personAddress);

            var entity = _personAddressRepository.GetByIdInclude(_personAddress.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Address.Address1));
        }

        [TestMethod]
        public void PersonAddressRepositoryTests_GetById()
        {
            var firstOrDefault = _personAddressRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _personAddressRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void PersonAddressRepositoryTests_GetAll()
        {
            var entityList = _personAddressRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

