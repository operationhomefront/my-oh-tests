﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories.EmailAddresses;
using OH.DAL.Repositories.Persons;
using OH.Net;

namespace MyOH.Net.Tests.Persons
{
    /// <summary>
    ///     Summary description for PersonEmailRepositoryTests
    /// </summary>
    [TestClass]
    public class PersonEmailRepositoryTests
    {
        private PersonEmailAddress _personEmailAddress;
        private IPersonEmailAddressRepository _personEmailAddressRepository;
        private IEmailAddressRepository _emailAddressRepository;
        private IPersonRepository _personRepository;
        private Person _person;
        private EmailAddress _emailAddress;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext PersonEmailAddressRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void personEmailAddressRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void personEmailAddressRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void personEmailAddressRepositoryTests_Initialize()
        {
            _personEmailAddressRepository = UnityConfig.GetConfiguredContainer().Resolve<IPersonEmailAddressRepository>();
            _personRepository = UnityConfig.GetConfiguredContainer().Resolve<IPersonRepository>();
            _emailAddressRepository = UnityConfig.GetConfiguredContainer().Resolve<IEmailAddressRepository>();

            _personEmailAddress = new PersonEmailAddress();
            _person = new Person { FirstName = "Hilario" };
            _emailAddress = new EmailAddress { Email = "hilario.urquieta@operationhomefront.net" };


            _personEmailAddress = new PersonEmailAddress
            {
                Person = _person,
                EmailAddress =  _emailAddress
            };

        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void personEmailAddressRepositoryTests_Save()
        {
            _personEmailAddressRepository.AddOrUpdate(_personEmailAddress);
            _personEmailAddressRepository.Save();

            var entity = _personEmailAddressRepository.GetById(_personEmailAddress.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void personEmailAddressRepositoryTests_Update()
        {
            _personEmailAddressRepository.AddOrUpdate(_personEmailAddress);
            _personEmailAddressRepository.Save();


            //Set this to nested entity, and update the name or another property
            var originalEntityValue = _personEmailAddress.EmailAddress.Email;

            var emailAddress = new EmailAddress { Email = "john.doe@operationhomefront.net"};


            //Set a new nested entity id
            _personEmailAddress.EmailAddress = emailAddress;


            _personEmailAddressRepository.AddOrUpdate(_personEmailAddress);

            var entity = _personEmailAddressRepository.GetByIdInclude(_personEmailAddress.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.EmailAddress.Email));
        }

        [TestMethod]
        public void personEmailAddressRepositoryTests_GetById()
        {
            var firstOrDefault = _personEmailAddressRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _personEmailAddressRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void personEmailAddressRepositoryTests_GetAll()
        {
            var entityList = _personEmailAddressRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

