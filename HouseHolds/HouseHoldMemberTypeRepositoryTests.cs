﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.HouseHolds;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Domain.Persons;
using OH.DAL.Repositories.HouseHolds;
using OH.Net;

namespace MyOH.Net.Tests.HouseHolds
{
    /// <summary>
    ///     Summary description for HouseHoldMemberTypeRepositoryTests
    /// </summary>
    [TestClass]
    public class HouseHoldMemberTypeRepositoryTests
    {
        private HouseHoldMemberListType _houseHoldMemberListType;
        private IHouseHoldMemberListTypeRepository _houseHoldMemberListTypeRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext HouseHoldMemberTypeRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void HouseHoldMemberTypeRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void HouseHoldMemberTypeRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void HouseHoldMemberTypeRepositoryTests_Initialize()
        {
            _houseHoldMemberListTypeRepository = UnityConfig.GetConfiguredContainer().Resolve<IHouseHoldMemberListTypeRepository>();

            _houseHoldMemberListType = new HouseHoldMemberListType{ HouseHoldMember = new HouseHoldMember
            {
                Person = new Person { FirstName = "Hilario", Lastname = "Urquieta"},                
            }, ListType = new ListType{ Name = "Veteran" }};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void HouseHoldMemberTypeRepositoryTests_Save()
        {
            _houseHoldMemberListTypeRepository.AddOrUpdate(_houseHoldMemberListType);
            _houseHoldMemberListTypeRepository.Save();

            var entity = _houseHoldMemberListTypeRepository.GetById(_houseHoldMemberListType.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void HouseHoldMemberTypeRepositoryTests_Update()
        {
            _houseHoldMemberListTypeRepository.AddOrUpdate(_houseHoldMemberListType);
            _houseHoldMemberListTypeRepository.Save();

            var originalEntityValue = _houseHoldMemberListType.ListType.Name;
            _houseHoldMemberListType.ListType.Name = "Retired Officer";


            _houseHoldMemberListTypeRepository.AddOrUpdate(_houseHoldMemberListType);
            var entity = _houseHoldMemberListTypeRepository.GetById(_houseHoldMemberListType.Id);


            Assert.IsFalse(originalEntityValue.Equals(entity.ListType.Name));
        }

        [TestMethod]
        public void HouseHoldMemberTypeRepositoryTests_GetById()
        {
            var firstOrDefault = _houseHoldMemberListTypeRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _houseHoldMemberListTypeRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void HouseHoldMemberTypeRepositoryTests_GetAll()
        {
            var entityList = _houseHoldMemberListTypeRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

