﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.HouseHolds;
using OH.DAL.Repositories.HouseHolds;
using OH.Net;

namespace MyOH.Net.Tests.HouseHolds
{
    /// <summary>
    ///     Summary description for HouseHoldRepositoryTests
    /// </summary>
    [TestClass]
    public class HouseHoldRepositoryTests
    {
        private HouseHold _houseHold;
        private IHouseHoldRepository _houseHoldRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext HouseHoldRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void HouseHoldRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void HouseHoldRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void HouseHoldRepositoryTests_Initialize()
        {
            _houseHoldRepository = UnityConfig.GetConfiguredContainer().Resolve<IHouseHoldRepository>();

          
            _houseHold = new HouseHold
            {
                Name = "Hilario's House Hold",

            };

        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void HouseHoldRepositoryTests_Save()
        {
            _houseHoldRepository.AddOrUpdate(_houseHold);
            _houseHoldRepository.Save();

            var entity = _houseHoldRepository.GetById(_houseHold.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void HouseHoldRepositoryTests_Update()
        {
            _houseHoldRepository.AddOrUpdate(_houseHold);
            _houseHoldRepository.Save();


            var originalEntityValue = _houseHold.Name;
            _houseHold.Name = "Lalo's";

            _houseHoldRepository.AddOrUpdate(_houseHold);

            HouseHold entity = _houseHoldRepository.GetById(_houseHold.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Name));
        }

        [TestMethod]
        public void HouseHoldRepositoryTests_GetById()
        {
            var firstOrDefault = _houseHoldRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _houseHoldRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void HouseHoldRepositoryTests_GetAll()
        {
            var entityList = _houseHoldRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}