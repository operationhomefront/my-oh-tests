﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.HouseHolds;
using OH.DAL.Repositories.HouseHolds;
using OH.Net;

namespace MyOH.Net.Tests.HouseHolds
{
    /// <summary>
    ///     Summary description for HouseHoldMemberRepositoryTests
    /// </summary>
    [TestClass]
    public class HouseHoldMemberRepositoryTests
    {
        private HouseHoldMember _houseHoldMember;
        private IHouseHoldMemberRepository _houseHoldMemberRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext HouseHoldMemberRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void HouseHoldMemberRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void HouseHoldMemberRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void HouseHoldMemberRepositoryTests_Initialize()
        {
            _houseHoldMemberRepository = UnityConfig.GetConfiguredContainer().Resolve<IHouseHoldMemberRepository>();

            _houseHoldMember = new HouseHoldMember{Grade = "5"};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void HouseHoldMemberRepositoryTests_Save()
        {
            _houseHoldMemberRepository.AddOrUpdate(_houseHoldMember);
            _houseHoldMemberRepository.Save();

            var entity = _houseHoldMemberRepository.GetById(_houseHoldMember.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void HouseHoldMemberRepositoryTests_Update()
        {
            _houseHoldMemberRepository.AddOrUpdate(_houseHoldMember);
            _houseHoldMemberRepository.Save();

            var originalEntityValue = _houseHoldMember.Grade;
            _houseHoldMember.Grade = "6";


            _houseHoldMemberRepository.AddOrUpdate(_houseHoldMember);
            var entity = _houseHoldMemberRepository.GetById(_houseHoldMember.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Grade));
        }

        [TestMethod]
        public void HouseHoldMemberRepositoryTests_GetById()
        {
            var firstOrDefault = _houseHoldMemberRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _houseHoldMemberRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void HouseHoldMemberRepositoryTests_GetAll()
        {
            var entityList = _houseHoldMemberRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

