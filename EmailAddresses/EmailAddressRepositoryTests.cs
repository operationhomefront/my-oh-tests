﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.EmailAddresses;
using OH.DAL.Repositories.EmailAddresses;
using OH.Net;

namespace MyOH.Net.Tests.EmailAddresses
{
    /// <summary>
    ///     Summary description for EmailAddressRepositoryTests
    /// </summary>
    [TestClass]
    public class EmailAddressRepositoryTests
    {
        private EmailAddress _emailAddress;
        private IEmailAddressRepository _emailAddressRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext EmailAddressRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void EmailAddressRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void EmailAddressRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void EmailAddressRepositoryTests_Initialize()
        {
            _emailAddressRepository = UnityConfig.GetConfiguredContainer().Resolve<IEmailAddressRepository>();

            _emailAddress = new EmailAddress{Email = "johndoe@somewhere.net"};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void EmailAddressRepositoryTests_Save()
        {
            _emailAddressRepository.AddOrUpdate(_emailAddress);
            _emailAddressRepository.Save();

            var entity = _emailAddressRepository.GetById(_emailAddress.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void EmailAddressRepositoryTests_Update()
        {
            var entity = _emailAddressRepository.GetById(_emailAddress.Id);
            if (entity == null) return;

            var originalEntityValue = entity.Email;
            entity.Email = "hurquieta@somenet.com";

            _emailAddressRepository.AddOrUpdate(entity);

            Assert.IsTrue(!originalEntityValue.Equals(entity.Email));
        }

        [TestMethod]
        public void EmailAddressRepositoryTests_GetById()
        {
            var firstOrDefault = _emailAddressRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _emailAddressRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void EmailAddressRepositoryTests_GetAll()
        {
            var entityList = _emailAddressRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

