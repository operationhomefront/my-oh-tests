﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Applications;
using OH.DAL.Domain.Attachments;
using OH.DAL.Repositories.Applications;
using OH.Net;

namespace MyOH.Net.Tests.Applications
{
    /// <summary>
    ///     Summary description for ApplicationClassRepositoryTests
    /// </summary>
    [TestClass]
    public class ApplicationClassRepositoryTests
    {
        private ApplicationClass _applicationClass;
        private IApplicationClassRepository _applicationClassRepository;
        private Application _application;
        //private NestedEntity2 _nestedEntity2;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext ApplicationClassRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void ApplicationClassRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void ApplicationClassRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void ApplicationClassRepositoryTests_Initialize()
        {
            _applicationClassRepository = UnityConfig.GetConfiguredContainer().Resolve<IApplicationClassRepository>();

            _applicationClass = new ApplicationClass();
            _application = new Application{Name = "Hearts of Valor"};
            
            _applicationClass = new ApplicationClass 
               {
                   Application = _application,
                   EntityClassGuid = Attachment.ClassGuid
               };

        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void ApplicationClassRepositoryTests_Save()
        {
            _applicationClassRepository.AddOrUpdate(_applicationClass);
            _applicationClassRepository.Save();

            var entity = _applicationClassRepository.GetById(_applicationClass.Id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void ApplicationClassRepositoryTests_Update()
        {
            _applicationClassRepository.AddOrUpdate(_applicationClass);
            _applicationClassRepository.Save();

            //Set this to nested entity, and update the name or another property
            var originalEntityValue = _applicationClass.Application.Name;

            var application = new Application { Name = "Hearts of Valor 2", NameAbbreviation = "HOV2"};

            //Set a new nested entity id
            _applicationClass.Application = application;
            
            _applicationClassRepository.AddOrUpdate(_applicationClass);

            var entity = _applicationClassRepository.GetByIdInclude(_applicationClass.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Application.Name));
        }

        [TestMethod]
        public void ApplicationClassRepositoryTests_GetById()
        {
            var firstOrDefault = _applicationClassRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _applicationClassRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void ApplicationClassRepositoryTests_GetAll()
        {
            var entityList = _applicationClassRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

