﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Applications;
using OH.DAL.Repositories.Applications;
using OH.Net;

namespace MyOH.Net.Tests.Applications
{
    /// <summary>
    ///     Summary description for ApplicationRepositoryTests
    /// </summary>
    [TestClass]
    public class ApplicationRepositoryTests
    {
        private Application _application;
        private IApplicationRepository _applicationRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext ApplicationRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void ApplicationRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void ApplicationRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void ApplicationRepositoryTests_Initialize()
        {
            _applicationRepository = UnityConfig.GetConfiguredContainer().Resolve<IApplicationRepository>();

            _application = new Application{Name = "Hearts of Valor", NameAbbreviation = "HOV"};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void ApplicationRepositoryTests_Save()
        {
            var id = _applicationRepository.AddOrUpdate(_application);
            _applicationRepository.Save();

            var entity = _applicationRepository.GetById(id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void ApplicationRepositoryTests_Update()
        {

            _applicationRepository.AddOrUpdate(_application);
            _applicationRepository.Save();

            var originalEntityValue = _application.Name;

            //Change a property here
            _application.Name = "Tons of Hearts of Valor";


            _applicationRepository.AddOrUpdate(_application);

            var entity = _applicationRepository.GetById(_application.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Name));
        }

        [TestMethod]
        public void ApplicationRepositoryTests_GetById()
        {
            var firstOrDefault = _applicationRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _applicationRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void ApplicationRepositoryTests_GetAll()
        {
            var entityList = _applicationRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

