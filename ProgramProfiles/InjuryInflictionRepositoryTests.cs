﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.ProgramProfile.Injury;
using OH.DAL.Domain.ProgramProfile.Military;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Repositories.Persons;
using OH.DAL.Repositories.ProgramProfiles.Injury;
using OH.DAL.Repositories.ProgramProfiles.Military;
using OH.Net;

namespace MyOH.Net.Tests.ProgramProfiles
{
    /// <summary>
    ///     Summary description for InjuryInflictionRepositoryTestsRepositoryTests
    /// </summary>
    [TestClass]
    public class InjuryInflictionRepositoryTests
    {
        private InjuryInfliction _injuryInfliction;
        private IInjuryInflictionRepository _injuryInflictionRepository;
        private IInjuryInflictionCategoryRepository _inflictionCatRepository;
        private IListMilitaryCombatTheaterRepository _listMilitaryCombatTheaterRepository;
        private IProfileMilitaryRepository _profileMilitaryRepository;
        private IPersonRepository _PersonRepository;
        private IProfileMilitaryAttachmentRepository _profileMilitaryAttachmentRepository;
        private ProfileMilitaryAttachment _profileMilitaryAttachment;
        private Attachment _attachment;
        private static string _base64String;


        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext InjuryInflictionRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void InjuryInflictionRepositoryTestsRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void InjuryInflictionRepositoryTestsRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void InjuryInflictionRepositoryTests_Initialize()
        {
            _injuryInflictionRepository = UnityConfig.GetConfiguredContainer().Resolve<IInjuryInflictionRepository>();
            _profileMilitaryAttachmentRepository = UnityConfig.GetConfiguredContainer().Resolve<IProfileMilitaryAttachmentRepository>();
            
            _injuryInfliction = new InjuryInfliction
            {
                //Id = Guid.NewGuid(),
                BriefDescription = "Test",
                DetailDescription= "Testing Details Description",
                TypeOfInjury = Guid.NewGuid(),
                Cause = "Test Cause",
                DateOfOccurrence = new DateTime(1970,1,1),
                Disposition = Guid.NewGuid(),
                LocationId = new Guid("869E2412-F6EF-462D-9BA5-CC6B501951B6"),
                InjuryInflictionCategoryId = new Guid("DC254B57-DC1D-4511-9051-20A8A8E50485"),
            };

            var inFile = new StreamReader(@"G:\OHDevelopment\Websites\MyOH.Net\MyOH.Net.Tests\Attachments\test.txt",
               System.Text.Encoding.ASCII);

            var base64CharArray = new char[inFile.BaseStream.Length];

            inFile.Read(base64CharArray, 0, (int)inFile.BaseStream.Length);

            _base64String = new string(base64CharArray);

            _attachment = new Attachment { FileName = "test.txt", FileExtension = ".txt", FileStream = Convert.FromBase64String(_base64String) };

        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void InjuryInflictionRepositoryTests_Save()
        {
            _profileMilitaryRepository = UnityConfig.GetConfiguredContainer().Resolve<IProfileMilitaryRepository>();
            _PersonRepository = UnityConfig.GetConfiguredContainer().Resolve<IPersonRepository>();
            Guid profileId = Guid.NewGuid();
            Guid servicemember = _PersonRepository.GetLatest().Id;
            var profile = _profileMilitaryRepository.AddOrUpdate(new ProfileMilitary
            {
                Id = profileId,
                ServiceMemberId = servicemember,
                BranchOfServiceId = new Guid("E46A61D6-B4F6-4730-A3CC-66444E3A87F9"),
                MilitaryRankId = new Guid("9EF50BEE-97A4-4EBC-8CB4-5BDCF081FEBE"),
                DeploymentStatusId = new Guid("1C630F3E-7F55-488A-9047-2F0DC2307E67"),
                VeteranStatusId = new Guid("44ECA156-3C17-4CA1-87CA-2F32BBFECE6A"),
                ServiceStatusId = new Guid("4D30B760-3C91-494E-86F2-60A25116A7BE"),
                TimeStamp = new TimeStamp() { CreatedBy = "Profile Created Test", RefId = profileId },
            });

            _profileMilitaryRepository.Save();

            _injuryInfliction.ProfileMilitaryId = profile;
            _injuryInfliction.TimeStamp = new TimeStamp() { CreatedBy = "Roy Created Test", RefId = _injuryInfliction.Id };

            _injuryInflictionRepository.Add(_injuryInfliction);
            _injuryInflictionRepository.Save();

            var targetProfile = _profileMilitaryRepository.GetById(profileId);

            _profileMilitaryAttachment = new ProfileMilitaryAttachment { ProfileMilitary = targetProfile, Attachment = _attachment };
            
            targetProfile.Documents = new List<ProfileMilitaryAttachment>{_profileMilitaryAttachment};
            _profileMilitaryRepository.Save();
            
            
            var entity = _injuryInflictionRepository.GetById(_injuryInfliction.Id);





            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void InjuryInflictionRepositoryTests_Update()
        {
            _injuryInfliction = _injuryInflictionRepository.GetLatest();

            //_injuryInflictionRepository.AddOrUpdate(_injuryInfliction);
            //_injuryInflictionRepository.Save();

            var originalEntityValue = _injuryInfliction.TypeOfInjury;
            _injuryInfliction.TypeOfInjury = Guid.NewGuid();
            _injuryInfliction.TimeStamp = new TimeStamp(){LastModifiedBy="Roy Modified Test",RefId =_injuryInfliction.Id};
            
            _injuryInflictionRepository.AddOrUpdate(_injuryInfliction);
            var entity = _injuryInflictionRepository.GetById(_injuryInfliction.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Id));
        }

        [TestMethod]
        public void InjuryInflictionRepositoryTests_GetById()
        {
            var firstOrDefault = _injuryInflictionRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _injuryInflictionRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void InjuryInflictionRepositoryTests_GetAll()
        {
            var entityList = _injuryInflictionRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

