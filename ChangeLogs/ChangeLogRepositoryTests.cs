﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.ChangeLogs;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Repositories.ChangeLogs;
using OH.Net;

namespace MyOH.Net.Tests.ChangeLogs
{
    /// <summary>
    /// Summary description for ChangeLogRepositoryTests
    /// </summary>
    [TestClass]
    public class ChangeLogRepositoryTests
    {
        private IChangeLogRepository _changeLogRepository;
        private ChangeLog _changeLog;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext ChangeLogRepositoryTestsContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        //public static void ChangeLogRepositoryTestsInitialize(TestContext testContext) {
       
            
        // }
        

        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void ChangeLogRepositoryTests_Cleanup() 
        { 
            //To avoid creating very large data sets
        }
        
        // Use TestInitialize to run code before running each test 
         [TestInitialize]
        public void ChangeLogRepositoryTests_Initialize() 
         {
             _changeLogRepository = UnityConfig.GetConfiguredContainer().Resolve<IChangeLogRepository>();
             _changeLog = new ChangeLog {EntityGuid = Guid.NewGuid(), Comment =  "hello world", TimeStamp = new TimeStamp{CreatedBy = "Hilario"}};
             
         }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
         // public void AttachmentRepositoryTests_Cleanup() { }
        //
        #endregion
         //[TestMethod]
         //public void ChangeLogRepositoryTests_GetAllEntitiesByGuid()
         //{
         //    var id = _attachmentRepository.GetAllEntitiesByGuid(Attachment.ClassGuid);             

         //    Assert.IsFalse(false);
         //}

        [TestMethod]
         public void ChangeLogRepositoryTests_Save()
        {
            _changeLogRepository.AddOrUpdate(_changeLog);
            _changeLogRepository.Save();


            var entity = _changeLogRepository.GetById(_changeLog.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void ChangeLogRepositoryTests_Update()
        {
            _changeLogRepository.AddOrUpdate(_changeLog);
            _changeLogRepository.Save();

            var originalEntityValue = _changeLog.Comment;

            _changeLog.Comment = "Good bye world";

            _changeLogRepository.AddOrUpdate(_changeLog);

            var entity = _changeLogRepository.GetById(_changeLog.Id);

            Assert.IsFalse(originalEntityValue.Equals(entity.Comment));
        }

        [TestMethod]
        public void ChangeLogRepositoryTests_GetById()
        {
            var firstOrDefault = _changeLogRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _changeLogRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void ChangeLogRepositoryTests_GetAll()
        {
            var entityList = _changeLogRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }      
    }
}
