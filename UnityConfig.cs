﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using OH.BLL.Attachments;
using OH.DAL.Repositories;
using OH.DAL.Repositories.Attachments;
using OH.DAL.Repositories.Categories;
using OH.DAL.Repositories.ChangeLogs;

namespace OH.Net.Tests
{
    public class UnityConfig
    {
        #region Unity Container

        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            container.LoadConfiguration();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        ///     Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }

        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        ///     There is no need to register concrete types such as controllers or API controllers (unless you want to
        ///     change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            //Attachment
            container.RegisterType<IAttachmentRepository, AttachmentRepository>();
            container.RegisterType<IAttachmentBllService, AttachmentBllService>();

            //Category
            container.RegisterType<ICategoryRepository, CategoryRepository>();

            //ChangeLog
            container.RegisterType<IChangeLogRepository, ChangeLogRepository>();
        }
    }
}
