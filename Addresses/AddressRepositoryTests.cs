﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Addresses;
using OH.DAL.Repositories.Addresses;
using OH.Net;

namespace MyOH.Net.Tests.Addresses
{
    /// <summary>
    ///     Summary description for AddressRepositoryTests
    /// </summary>
    [TestClass]
    public class AddressRepositoryTests
    {
        private Address _address;
        private IAddressRepository _addressRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext AddressRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void AddressRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void AddressRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void AddressRepositoryTests_Initialize()
        {
            _addressRepository = UnityConfig.GetConfiguredContainer().Resolve<IAddressRepository>();

            _address = new Address{Address1 = "1234 Medical Drive",City = "San Antonio", State = "TX"};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void AddressRepositoryTests_Save()
        {
            var id = _addressRepository.AddOrUpdate(_address);
            _addressRepository.Save();

            var entity = _addressRepository.GetById(id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void AddressRepositoryTests_Update()
        {
            var entity = _addressRepository.GetAll().FirstOrDefault();
            if (entity == null) return;

            var originalEntityValue = _address.Address1;
            entity.Address1 = "5678 West Over Hills";

            _addressRepository.AddOrUpdate(entity);
            entity = _addressRepository.GetById(entity.Id);

            Assert.IsTrue(!originalEntityValue.Equals(entity.Address1));
        }

        [TestMethod]
        public void AddressRepositoryTests_GetById()
        {
            var firstOrDefault = _addressRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _addressRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void AddressRepositoryTests_GetAll()
        {
            var entityList = _addressRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

