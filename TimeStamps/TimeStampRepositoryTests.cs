﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.TimeStamps;
using OH.DAL.Repositories.TimeStamps;
using OH.Net;

namespace MyOH.Net.Tests.TimeStamps
{
    /// <summary>
    ///     Summary description for TimeStampRepositoryTests
    /// </summary>
    [TestClass]
    public class TimeStampRepositoryTests
    {
        private TimeStamp _timeStamp;
        private ITimeStampRepository _timeStampRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TimeStampRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void TimeStampRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void TimeStampRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void TimeStampRepositoryTests_Initialize()
        {
            _timeStampRepository = UnityConfig.GetConfiguredContainer().Resolve<ITimeStampRepository>();

            _timeStamp = new TimeStamp{CreatedBy = "Hilario", UtcCreatedDate = DateTime.UtcNow};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void TimeStampRepositoryTests_Save()
        {
            var id = _timeStampRepository.AddOrUpdate(_timeStamp);
            _timeStampRepository.Save();

            var entity = _timeStampRepository.GetById(id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void TimeStampRepositoryTests_Update()
        {
            TimeStamp entity = _timeStampRepository.GetAll().FirstOrDefault();
            if (entity == null) return;

            var originalEntityValue = entity.UtcCreatedDate;
            
            entity.UtcCreatedDate = DateTime.UtcNow;

            _timeStampRepository.AddOrUpdate(entity);

            Assert.IsTrue(!originalEntityValue.Equals(entity.UtcCreatedDate));
        }

        [TestMethod]
        public void TimeStampRepositoryTests_GetById()
        {
            TimeStamp firstOrDefault = _timeStampRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            Guid id = firstOrDefault.Id;

            TimeStamp entity = _timeStampRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void TimeStampRepositoryTests_GetAll()
        {
            List<TimeStamp> entityList = _timeStampRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

