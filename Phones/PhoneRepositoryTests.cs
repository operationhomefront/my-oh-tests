﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Phones;
using OH.DAL.Repositories.Phones;
using OH.Net;

namespace MyOH.Net.Tests.Phones
{
    /// <summary>
    ///     Summary description for PhoneRepositoryTests
    /// </summary>
    [TestClass]
    public class PhoneRepositoryTests
    {
        private Phone _phone;
        private IPhoneRepository _phoneRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext PhoneRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void PhoneRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void PhoneRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void PhoneRepositoryTests_Initialize()
        {
            _phoneRepository = UnityConfig.GetConfiguredContainer().Resolve<IPhoneRepository>();

            _phone = new Phone{PhoneNumber = "210-213-8454"};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void PhoneRepositoryTests_Save()
        {
            _phoneRepository.AddOrUpdate(_phone);
            _phoneRepository.Save();

            var entity = _phoneRepository.GetById(_phone.Id);


            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void PhoneRepositoryTests_Update()
        {
            _phoneRepository.AddOrUpdate(_phone);
            _phoneRepository.Save();

            var originalEntityValue = _phone.PhoneNumber;

            _phone.PhoneNumber = "210-123-4578";

            _phoneRepository.AddOrUpdate(_phone);

            var entity = _phoneRepository.GetById(_phone.Id);

            Assert.IsTrue(!originalEntityValue.Equals(entity.PhoneNumber));
        }

        [TestMethod]
        public void PhoneRepositoryTests_GetById()
        {
            Phone firstOrDefault = _phoneRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            Guid id = firstOrDefault.Id;

            Phone entity = _phoneRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void PhoneRepositoryTests_GetAll()
        {
            List<Phone> entityList = _phoneRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

