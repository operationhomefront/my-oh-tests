﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Categories;
using OH.DAL.Domain.ListTypes;
using OH.DAL.Repositories.Categories;
using OH.DAL.Repositories.ListTypes;
using OH.Net;

namespace MyOH.Net.Tests.Categories
{
    /// <summary>
    ///     Summary description for CategoryListTypeRepositoryTests
    /// </summary>
    [TestClass]
    public class CategoryListTypeRepositoryTests
    {
        private CategoryListType _categoryListType;
        private Category _category;
        private ListType _listType;
        private ICategoryListTypeRepository _categoryListTypeRepository;
        private ICategoryRepository _categoryRepository;
        private IListTypeRepository _listTypeRepository;

        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext CategoryListTypeRepositoryTestsContext { get; set; }

        #region Additional test attributes

        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize]
        //public static void CategoryListTypeRepositoryTests_Initialize(TestContext testContext)
        //{
        //}


        // Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup]
        //public static void CategoryListTypeRepositoryTests_Cleanup()
        //{
        //    //To avoid creating very large data sets
        //}

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void CategoryListTypeRepositoryTests_Initialize()
        {
            _categoryListTypeRepository = UnityConfig.GetConfiguredContainer().Resolve<ICategoryListTypeRepository>();
            _categoryRepository = UnityConfig.GetConfiguredContainer().Resolve<ICategoryRepository>();
            _listTypeRepository = UnityConfig.GetConfiguredContainer().Resolve<IListTypeRepository>();
            _category = new Category{Name = "TestCategory"};

            _listType = new ListType {Name = "TestList"};

            
            _categoryListType = new CategoryListType{Category = _category, ListType = _listType};


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //

        #endregion

        [TestMethod]
        public void CategoryListTypeRepositoryTests_Save()
        {
            _categoryListTypeRepository.AddOrUpdate(_categoryListType);
            _categoryListTypeRepository.Save();

            var entity = _categoryListTypeRepository.GetById(_categoryListType.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void CategoryListTypeRepositoryTests_Update()
        {
            _categoryListTypeRepository.AddOrUpdate(_categoryListType);
            _categoryListTypeRepository.Save();

            _categoryListType = _categoryListTypeRepository.GetByIdInclude(_categoryListType.Id);

            var originalEntityValue = _categoryListType.ListType.Name;
            
            var listType = new ListType { Name = "listTypeTest2" };

            _categoryListType.ListType = listType;

            _categoryListTypeRepository.AddOrUpdate(_categoryListType);

            var entity = _categoryListTypeRepository.GetByIdInclude(_categoryListType.Id);

            Assert.IsTrue(!originalEntityValue.Equals(entity.ListType.Name));
        }

        [TestMethod]
        public void CategoryListTypeRepositoryTests_GetById()
        {
            var firstOrDefault = _categoryListTypeRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _categoryListTypeRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void CategoryListTypeRepositoryTests_GetAll()
        {
            var entityList = _categoryListTypeRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}

