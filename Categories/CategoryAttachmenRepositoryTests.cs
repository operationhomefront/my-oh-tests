﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Attachments;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Attachments;
using OH.DAL.Repositories.Categories;
using OH.Net;

namespace MyOH.Net.Tests.Categories
{
    /// <summary>
    /// Summary description for CategoryAttachmenRepositoryTests
    /// </summary>
    [TestClass]
    public class CategoryAttachmenRepositoryTests
    {
        private ICategoryAttachmentRepository _categoryAttachmentRepository;
        private IAttachmentRepository _attachmentRepository;
        private ICategoryRepository _categoryRepository;
        private Attachment _attachment;
        private static string _base64String;
        private CategoryAttachment _categoryAttachment;
        private Category _categoryNode0;
        private Category _categoryNode1;
        private Category _categoryNode2;
        private Category _categoryNode3;
        private Category _categoryNode4;
        private Category _categoryNode5;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext CategoryAttachmentRepositoryTestsContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        //public static void CategoryAttachmentRepositoryTestsInitialize(TestContext testContext) {


        // }


        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void CategoryAttachmentRepositoryTests_Cleanup()
        {
            //To avoid creating very large data sets
        }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void CategoryAttachmentRepositoryTests_Initialize()
        {
            _categoryAttachmentRepository = UnityConfig.GetConfiguredContainer().Resolve<ICategoryAttachmentRepository>();
            _attachmentRepository = UnityConfig.GetConfiguredContainer().Resolve<IAttachmentRepository>();
            _categoryRepository = UnityConfig.GetConfiguredContainer().Resolve<ICategoryRepository>();


            var inFile = new StreamReader(@"G:\OHDevelopment\Websites\MyOH.Net\MyOH.Net.Tests\Attachments\test.txt",
                 System.Text.Encoding.ASCII);

            var base64CharArray = new char[inFile.BaseStream.Length];

            inFile.Read(base64CharArray, 0, (int)inFile.BaseStream.Length);

            _base64String = new string(base64CharArray);        

            _attachment = new Attachment { FileName = "test.txt", FileExtension = ".txt", FileStream = Convert.FromBase64String(_base64String) };
                                
            _categoryAttachment = new CategoryAttachment
            {
                Attachment = _attachment,
                Category = _categoryNode0
            };

            //Seeding for Development
            BuildCategoryNodeTree();

            SaveAttachmentsList();


        }

        private void SaveAttachmentsList()
        {
            var attachmentsList = new List<Attachment>
            {
                new Attachment
                {
                    FileName = "mypdf.pdf",
                    FileExtension = ".pdf",
                    FileStream = Convert.FromBase64String(_base64String)
                },
                new Attachment
                {
                    FileName = "myHTML.html",
                    FileExtension = ".html",
                    FileStream = Convert.FromBase64String(_base64String)
                },
                new Attachment
                {
                    FileName = "myJPEG.jpeg",
                    FileExtension = ".jpeg",
                    FileStream = Convert.FromBase64String(_base64String)
                }
            };

            foreach (var attachmentId in attachmentsList.Select(attachment => _attachmentRepository.AddOrUpdate(attachment)))
            {
                _attachmentRepository.Save();
                var categoryId = _categoryRepository.GetCategoryByName(_categoryNode3.Name).Id;

                _categoryAttachment = new CategoryAttachment
                {
                    AttachmentId = attachmentId,
                    CategoryId = categoryId
                };
                _categoryAttachmentRepository.AddOrUpdate(_categoryAttachment);
                _categoryAttachmentRepository.Save();
            }
        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void AttachmentRepositoryTests_Cleanup() { }
        //
        #endregion
        //[TestMethod]
        //public void CategoryAttachmentRepositoryTests_GetAllEntitiesByGuid()
        //{
        //    var id = _attachmentRepository.GetAllEntitiesByGuid(Attachment.ClassGuid);             

        //    Assert.IsFalse(false);
        //}

        private void BuildCategoryNodeTree()
        {

            //Category Tree to test the creation of a catery tree
            _categoryNode0 = new Category { Name = String.Format("Node0_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode1 = new Category { Name = String.Format("Node1_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode2 = new Category { Name = String.Format("Node2_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode3 = new Category { Name = String.Format("Node3_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode4 = new Category { Name = String.Format("Node4_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode5 = new Category { Name = String.Format("Node5_{0}", Guid.NewGuid().ToString().Replace("-", "")) };

            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode1, _categoryNode0);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode2, _categoryNode0);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode3, _categoryNode1);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode4, _categoryNode2);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode5, _categoryNode1);
            _categoryRepository.Save();
        }

        [TestMethod]
        public void CategoryAttachmentRepositoryTests_Save()
        {                        
           _categoryAttachmentRepository.AddOrUpdate(_categoryAttachment);
            _categoryAttachmentRepository.Save();

            var entity = _categoryAttachmentRepository.GetById(_categoryAttachment.Id);
            
            
            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void CategoryAttachmentRepositoryTests_Update()
        {
            _categoryAttachmentRepository.AddOrUpdate(_categoryAttachment);
            _categoryAttachmentRepository.Save();

            _categoryAttachment = _categoryAttachmentRepository.GetByIdInclude(_categoryAttachment.Id);

            var originalEntityValue = _categoryAttachment.Attachment.FileName;

            var category = new Category { Name = "UpdatedFileName.jpg" };     

            _categoryAttachment.Category = category;

            _categoryAttachmentRepository.AddOrUpdate(_categoryAttachment);

            var entity = _categoryAttachmentRepository.GetByIdInclude(_categoryAttachment.Id);

            Assert.IsTrue(!originalEntityValue.Equals(entity.Category.Name));
        }

        [TestMethod]
        public void CategoryAttachmentRepositoryTests_GetById()
        {
            var firstOrDefault = _categoryAttachmentRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _categoryAttachmentRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void CategoryAttachmentRepositoryTests_GetAll()
        {
            var entityList = _categoryAttachmentRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }

    }
}
