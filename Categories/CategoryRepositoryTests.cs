﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OH.DAL.Domain.Categories;
using OH.DAL.Repositories.Categories;
using OH.Net;

namespace MyOH.Net.Tests.Categories
{
    /// <summary>
    /// Summary description for CategoryRepositoryTests
    /// </summary>
    [TestClass]
    public class CategoryRepositoryTests
    {
        private ICategoryRepository _categoryRepository;
        private Category _category;
        private Category _categoryNode0;
        private Category _categoryNode1;
        private Category _categoryNode2;
        private Category _categoryNode3;
        private Category _categoryNode4;
        private Category _categoryNode5;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext CategoryRepositoryTestsContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        //public static void CategoryRepositoryTestsInitialize(TestContext testContext) {


        // }


        // Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup]
        public static void CategoryRepositoryTests_Cleanup()
        {
            //To avoid creating very large data sets
        }

        // Use TestInitialize to run code before running each test 
        [TestInitialize]
        public void CategoryRepositoryTests_Initialize()
        {
            _categoryRepository = UnityConfig.GetConfiguredContainer().Resolve<ICategoryRepository>();
            
            //This is a basic category to test the creation of a category
            _category = new Category { Name = String.Format("My Category_{0}",Guid.NewGuid().ToString().Replace("-",""))};
            


        }

        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void AttachmentRepositoryTests_Cleanup() { }
        //
        #endregion
        //[TestMethod]
        //public void CategoryRepositoryTests_GetAllEntitiesByGuid()
        //{
        //    var id = _attachmentRepository.GetAllEntitiesByGuid(Attachment.ClassGuid);             

        //    Assert.IsFalse(false);
        //}



        [TestMethod]
        public void CategoryRepositoryTests_GetCategoryByIdIncludingSubCategories()
        {
            BuildCategoryNodeTree();
            _categoryRepository.Save();

            var entity = _categoryRepository.GetCategoryByIdInclude(_categoryNode0.Id);

            Assert.IsTrue(entity.SubCategories.Any());
        }

        [TestMethod]
        public void CategoryRepositoryTests_GetSubCategoriesFromParentCategory()
        {
            BuildCategoryNodeTree();

            var subcategories = _categoryRepository.GetCategoryByIdInclude(_categoryNode0.Id);

            Assert.IsNotNull(subcategories);
        }

        [TestMethod]
        public void CategoryRepositoryTests_AddSubCategoryToCategory()
        {         
            BuildCategoryNodeTree();            

            Assert.IsFalse(_categoryNode5.ParentCategory.Id.Equals(Guid.Empty));
        }

        private void BuildCategoryNodeTree()
        {
            _categoryNode0 = new Category { Name = String.Format("Node0_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode1 = new Category { Name = String.Format("Node1_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode2 = new Category { Name = String.Format("Node2_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode3 = new Category { Name = String.Format("Node3_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode4 = new Category { Name = String.Format("Node4_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryNode5 = new Category { Name = String.Format("Node5_{0}", Guid.NewGuid().ToString().Replace("-", "")) };
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode1, _categoryNode0);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode2, _categoryNode0);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode3, _categoryNode1);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode4, _categoryNode2);
            _categoryRepository.AddOrUpdateSubCategoryParentCategory(_categoryNode5, _categoryNode1);
            _categoryRepository.Save();

        }

        [TestMethod]
        public void CategoryRepositoryTests_Save()
        {
            _categoryRepository.AddOrUpdate(_category);
            _categoryRepository.Save();

            var entity = _categoryRepository.GetById(_category.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void CategoryRepositoryTests_Update()
        {
            _categoryRepository.AddOrUpdate(_category);
            _categoryRepository.Save();

            _category.Name = String.Format("My Category Updated_{0}", Guid.NewGuid().ToString().Replace("-", ""));

            _categoryRepository.AddOrUpdate(_category);
            var entity = _categoryRepository.GetById(_category.Id);

            Assert.IsFalse(entity.Id.Equals(Guid.Empty));
        }

        [TestMethod]
        public void CategoryRepositoryTests_GetById()
        {
            var firstOrDefault = _categoryRepository.GetAll().FirstOrDefault();
            if (firstOrDefault == null) return;
            var id = firstOrDefault.Id;

            var entity = _categoryRepository.GetById(id);

            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public void CategoryRepositoryTests_GetAll()
        {
            var entityList = _categoryRepository.GetAll().ToList();

            Assert.IsTrue(entityList.Any());
        }
    }
}
